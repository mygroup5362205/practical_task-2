package com.kazmiruk.multiplication_table;

import java.util.ArrayList;
import java.util.List;

public class Multiplication<T extends Number> {

    private final Class<T> cls;

    public Multiplication(Class<T> cls) {
        this.cls = cls;
    }

    /**
     * Multiplies two number
     * series as matrices are multiplied by a matrix
     *
     * @param firstMultipliers with first factors
     * @param secondMultipliers with second factors
     * @return a two-dimensional list with the result of multiplication of numerical series
     */
    @SuppressWarnings("unchecked")
    public List<List<T>> multiply(List<T> firstMultipliers, List<T> secondMultipliers) {
        List<List<T>> afterMultiply = new ArrayList<>();

        for (T firstMultiplier : firstMultipliers) {
            List<T> row = new ArrayList<>(firstMultipliers.size());
            for (T secondMultiplier : secondMultipliers) {
                row.add((T) multiplyTwoNumbersAccordingToType(firstMultiplier, secondMultiplier));
            }
            afterMultiply.add(row);
        }
        return afterMultiply;
    }

    private Number multiplyTwoNumbersAccordingToType(Number firstMultiplier, Number secondMultiplier) {
        Number multiplyTwo;
        if (cls.equals(Integer.class)) {
            multiplyTwo = firstMultiplier.intValue() * secondMultiplier.intValue();
        } else {
            multiplyTwo = firstMultiplier.doubleValue() * secondMultiplier.doubleValue();
        }
        return multiplyTwo;
    }

}
